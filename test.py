import urllib.request as libreq
import feedparser
import pickle
from classes import Paper,Author

# Setup

# Remember old authors

try:
    auth_file = open("authors","rb")
    Author.authors = pickle.load(auth_file)
    auth_file.close()
except:
    pass

if __name__ == "__main__":

    # Base api query url
    base_url = 'http://export.arxiv.org/api/query?';

    # Search parameters
    search_query = 'cat:hep-th' # search query
    start = 0                     # retreive the first 5 results
    max_results = 5

    query = 'search_query=%s&start=%i&max_results=%i&sortBy=lastUpdatedDate&sortOrder=descending' % (search_query,
                                                        start,
                                                        max_results)

    # perform a GET request using the base_url and query
    with libreq.urlopen(base_url+query) as url:
        response = url.read()

    # parse the response using feedparser
    feed = feedparser.parse(response)

    # Run through each entry

    all_papers = []
    good_papers = []

    for entry in feed.entries:

        paper = Paper(entry.id.split('/abs/')[-1],entry.published,entry.title,[auth.name for auth in entry.authors],[t['term'] for t in entry.tags],entry.summary)

        all_papers.append(paper)
        if paper.eval()>0: good_papers.append(paper)

        print(paper.published.day)
        print(paper.abstract)
        print([(auth.name,auth.institution) for auth in paper.authors])
        print(paper.id)
        print(paper.tags)
        print(paper.title)
        print("-------------")

# End

with open("authors","wb") as auth_file:
    pickle.dump(Author.authors,auth_file)