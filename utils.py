from fuzzywuzzy import process as fuzzyprocess
import datetime

def fuzzymatch(target, matches, bool=True):
    match = fuzzyprocess.extractOne(target, matches)
    if match !=None and match[1] >90:
        if bool: return 1
        return match[0]
    if bool: return 0
    return None


def search_keyword(text, tokens):
    count = 0
    for token in tokens:
        count += text.count(token)
    return count

# Expects list ("token1 token2 ..", ....)
# All token1, token2, .. must appear to count +1
def search_correlate(text, tokens):
    count = 0
    for macro_token in tokens:
        micro_tokens = macro_token.split(" ")
        add = 1
        for micro in micro_tokens:
            add *= search_keyword(text,(micro,))
        if add>0: count+=1
    return count


# How do we handle time
# Example of published date: 2023-07-18T17:56:47Z

def toDate(str):
    cut = str.find("T")
    if cut>0: str = str[:cut]
    return datetime.datetime.strptime(str,"%Y-%m-%d").date()


if __name__ == "__main__":
    abstract = "We settle a long-standing question about the hypermultiplet moduli spaces of the heterotic strings on ALE singularities. These heterotic backgrounds are specified by the singularity type, an instanton number, and a (nontrivial) flat connection at infinity. Building on their interpretation as six-dimensional theories, we determine a class of three-dimensional N=4 quiver gauge theories whose quantum corrected Coulomb branch coincides with the exact heterotic hypermultiplet moduli space."
    print(search_keyword(abstract, ("string", "heterotic", "N=4")))
    print(search_correlate(abstract, ("moduli space", "moduli spac", "heterotic string theory", "hypermultiplet stuff")))# theory and stuff are both missing. Substring are also fine ->2
    print(fuzzymatch("Craig R. Clark", ("Craig Clark", "Chen Yang")))