# Interface to handle inspirehep search of author institution
# MADONNA BLATTA

import json
from requests import request

def getInstitution(auth_name):
    #with libreq.urlopen("https://inspirehep.net/api/authors?sort=bestmatch&size=1&page=1&q="+auth_name) as url:
    #    response = url.read()
    response = request(method="GET",url="https://inspirehep.net/api/authors?sort=bestmatch&size=1&page=1&q="+auth_name)
    data = response.json()
    return data['hits']['hits'][0]['metadata']['positions'][0]['institution']

if __name__ == "__main__":
    print("S.Hawking: ", getInstitution("Stephen Hawking"))