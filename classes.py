from inspire import getInstitution
import utils
import lists # this file has lists of authors I want to follow, or topics I'm interested in. As such, it is a bit personal and I won't commit it

class Paper:
    def __init__(self, id,published,title,authors,tags,abstract):
        self.id = id
        self.published = utils.toDate(published)
        self.title = title
        self.authors = [Author.getAuthor(auth) for auth in authors]
        self.tags = tags
        self.abstract = abstract
    
    def eval(self):

        value = 0

        value += 3* lists.search_author(self.authors)
        value += 1* lists.search_institution(self.authors)
        value += 4* lists.search_topic(self.title,self.abstract)

        return value

class Author:
    
    authors = {}

    def __init__(self,name,institution):
        self.name = name
        self.institution = institution
    
    @staticmethod
    def getAuthor(name):
        name_match = utils.fuzzymatch(name, list(Author.authors.keys()),False)
        if name_match !=None:
            return Author.authors.get(name_match)
        else:
            Author.authors[name] = Author(name, getInstitution(name))
        return Author.authors.get(name)


if __name__ == "__main__":
    paper = Paper("5", "2023-07-18T17:56:47Z", "Fuzzy dark matter solitons and halo stuff", ("Lam Hui", "Kfir Blum"), "tags", "Solitonic solutions of self gravitating massive scalar")
    print(Author.authors["Lam Hui"].institution)
    print("Evaluation: ", paper.eval())